menuButton = document.getElementById('menu-button');

menuButton.onclick = () => {
  document.getElementById('sidebar').style.display = 'block';
}

sidebar = document.getElementById('sidebar');
links = [].slice.call(sidebar.getElementsByTagName('a'));
links.forEach(
  (link) => link.onclick = (() => {
    if (window.getComputedStyle(document.getElementById('menu-button')).getPropertyValue('display') != 'none') {
      sidebar.style.display = 'none'
    }
  })
)