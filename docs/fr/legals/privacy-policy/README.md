# Politique de confidentialité

**Dernière mise à jour:** 9 Mars 2020

<a href="/legals/privacy-policy/Misakey-PrivacyPolicy-2020-03-09.pdf" download>Télécharger la version PDF</a>

La plateforme de gestion des données personnelles Misakey (le "**Service**") disponible via le site
Internet [misakey.com](https://www.misakey.com) (le "**Site Internet**") ainsi que le Site Internet
vous sont fournis par Misakey, société par actions simplifiée au capital de 12.500 euros,
immatriculée au RCS de Paris sous le n° 845 272 053, ayant son siège social au 66 avenue des Champs
Elysées, 75008 Paris (« "**nous**", "**notre**" ou "**nos**").


Vous pouvez nous contacter à l’adresse email suivante : [dpo@misakey.com](mailto:dpo@misakey.com)

Aux fins des présentes, "**vous**", "**votre**", "**vos**" ou "**l’Utilisateur**" désigne la
personne physique dont les données personnelles sont collectées pour être traitées au titre des
présentes, et revêtant la qualité de « personne concernée » au sens de la Législation sur les
Données Personnelles, telle que définie ci-après.


Les termes en majuscule figurant dans la présente politique de confidentialité (la “Politique”)
sont définis dans la Documentation Contractuelle accessible ici :
[about.misakey.com/#/fr/legals/tos](https://about.misakey.com/#/fr/legals/tos/)


Misakey est le « responsable de traitement » au sens de la Législation sur les Données Personnelles (i.e. la société qui est responsable du traitement de vos données personnelles).

## 1. Données personnelles collectées

### 1.1. Informations fournies par l’Utilisateur


Lorsque vous utilisez nos services :
* Navigation sur le Site Internet;
* Service Collaboratif de Création et de Modification des Fiches;
* Service de Consultation;
* Services de Gestion des Comptes Internet;
* Services Professionnelles

certaines de vos données personnelles telles que vos données d’identification (nom public, photo de
profil), vos coordonnées de contact (adresse email, numéro de téléphone), votre adresse IP, des
informations sur l’appareil utilisé, votre historique des interactions (demandes envoyées,
consentements donnés, historique de connexion…) avec les Organisations peuvent être traitées.

Certaines de vos données personnelles telles que vos données d’identification et vos coordonnées de
contact (nom public, photo de profil, adresse email, numéro de téléphone) nous serons également
fournies par vous lors de la création d’un Compte de Données Utilisateur ou d’un Compte Embarqué
afin que vous puissiez bénéficier de nos Services, ou lorsque vous nous contactez, quelle que soit
la raison ou le moyen utilisé.

Nous nous réservons le droit de conserver une archive de tous les contacts que vous prendriez avec
nous.

### 1.2. Informations automatiquement collectées

**Cookies :** Nous n’utilisons pas de cookies pour surveiller votre navigation sur le site internet.
Nous utilisons les cookies nécessaires pour vous fournir nos services. Nous utilisons aussi un
cookies “matomo” anonymisé afin d’agréger des données statistiques d’usage de nos services.

**Informations de vos appareils :** Nous pouvons collecter des informations sur vos appareils à
chaque fois que vous les utilisez pour accéder à nos Services.

## 2. Comment nous utilisons vos données personnelles

| Type de données personnelles | Base légale du traitement | Finalité |
| ---------------------------- | ------------------------- | -------- |
| E-mail et/ou téléphone | Bonne exécution de la relation contractuelle qui nous lie à vous | - Validation de votre identité pour l’utilisation de nos Services nécessitant un Compte de Données Utilisateur <br/>- Utilisation du Service <br />- Gestion des demandes de renseignements <br />- Envoie des notifications relatives à l’activité, la sécurité, l’évolution et l’évaluation de nos services |
| Nom public et photo de profil publique | Bonne exécution de la relation contractuelle qui nous lie à vous | Permettre à n’importe quel site internet ayant un identifiant “adresse e-mail” ou “téléphone” d’afficher le nom public et la photo de profil de l’utilisateur |
| Historique de connection, adresse IP et informations sur l’appareil utilisé | Bonne exécution de la relation contractuelle qui nous lie à vous | Analyses statistiques de l’usage du Service pour améliorer la sécurité et la qualité de l’ensemble de nos services |
| Historique des interactions avec les organisations | Bonne exécution de la relation contractuelle qui nous lie à vous | Analyses statistiques de l’usage du Service pour améliorer la sécurité et la qualité de l’ensemble de nos services |

## 3. Marketing et communications

Si vous nous en avez donné l’autorisation, nous vous contacterons par email à propos de l’histoire de l’aventure Misakey. Si vous veniez à ne plus souhaiter recevoir nos communications marketing, vous pourrez retirer votre consentement à tout moment à l’aide du lien ”désinscription” présent sur chaque communication marketing. Pour de plus amples informations, merci de vous rendre à la section « Vos droits » ci-dessous.

Le Site Internet peut contenir des liens hypertextes vers les sites internet de nos partenaires et de tiers. Nous vous prions de noter que si vous suivez ces liens, les sites internet et services fournis seront régis par leurs propres conditions d’utilisation et politiques de confidentialité. Nous ne pourrons en aucun cas être tenus responsables de la non-conformité de leurs conditions d’utilisation et politiques de confidentialité à la Législation sur les Données Personnelles. Nous vous conseillons de prendre connaissance des politiques de confidentialité et des conditions d’utilisation applicables à ces sites internet avant de fournir vos données personnelles et d’utiliser ces sites internet.

## 4. Divulgation de vos données personnelles

Dans le cadre des finalités exposées à la section 2, les seules données que nous sommes susceptibles de transférer sont vos identifiants à :
* Des tiers , seulement lorsque vous les sollicitez dans le cadre de l’utilisation de nos Services;
* Des prestataires informatiques, dans le but d’héberger vos données;
* Des représentants des forces de l’ordre, des autorités judiciaires ou administratives ou d’autres tiers habilités, sous réserve du respect de nos obligations déontologiques relatives à la confidentialité, en réponse à une demande de renseignements si nous croyons que la communication de ces informations est légale ou requise par la Législation sur les Données Personnelles.

## 5. Conservation sécurisée de vos données

Nous utilisons les dispositifs techniques et organisationnels appropriés pour protéger vos données personnelles, par exemple :
* L’accès à votre Compte de Données Utilisateur est contrôlé par un mot de passe et un  identifiant;
* Nous conservons vos données personnelles sur des serveurs sécurisés.

Même si nous nous engageons à déployer nos meilleurs efforts pour protéger vos données personnelles, vous reconnaissez que l’utilisation d’Internet n’est pas totalement sécurisée et que, dans ce cadre, nous ne pouvons pas garantir la sécurité ou l’intégrité de vos données personnelles fournies par vous ou qui vous ont été fournies via Internet.

## 6. Périmètre du transfert de vos données

Vos données personnelles sont traitées dans l’Union Européenne.

## 7. Vos droits

Vous disposez d’un droit d’accès, de rectification ou d’effacement, de limitation du traitement de vos données personnelles, d’un droit d’opposition et d’un droit à la portabilité de vos données.

Vous avez également le droit de définir des instructions concernant la conservation, l'effacement et la communication de vos données à caractère personnel après votre décès.

Lorsque le traitement est fondé sur votre consentement, vous disposez en outre du droit de le révoquer.

Vous pouvez exercer ces droits à tout moment en adressant votre demande accompagnée d’un justificatif d’identité à l’adresse email renseignée au début de la présente Politique.

Toute demande devra spécifier les données personnelles auxquelles vous souhaitez avoir accès, que vous souhaitez rectifier, effacer ou remplacer ou dont vous souhaitez limiter le traitement ou invoquer la portabilité. Les informations relatives à votre Compte de Données Utilisateur pourront vous être demandées pour faciliter le traitement de votre demande.

Vous pouvez également introduire une réclamation auprès de la Commission Nationale de l’Informatique et des Libertés si vous considérez que le traitement de vos données à caractère personnel porte atteinte à vos droits en matière de protection des données.

Vous pouvez choisir de ne plus recevoir de messages publicitaires de notre part en suivant les instructions figurant dans les messages qui vous sont envoyés.

## 8. Durée de conservation des données

Vos données seront conservées pendant une période de 5 ans pour la conservation des preuves selon les délais de prescription applicables.

## 9. Utilisation des cookies

### 9.1. Nos cookies

Un « cookie » est un petit fichier texte contenant des données qui sont enregistrées sur un terminal (ordinateur, tablette, smartphone…) lorsque vous naviguez sur un site Internet. Nous utilisons des cookies pour accélérer votre accès et rendre votre expérience de nos services nécessitant un compte utilisateur plus fonctionnelle et agréable en vous permettant une connexion automatique.

La durée de conservation de ces informations n'excède pas 13 mois.

### 9.2. Cookies fournis par des tiers

Nous n’utilisons actuellement pas de services tiers. Si nous étions amenés à souhaiter utiliser un service tiers, votre consentement explicite pour le dépôt du cookie associé vous serait alors demandé avec des précisions sur ce service tiers.

## 10. Modification de cette politique

Nous pouvons mettre à jour cette Politique à tout moment. Toute modification de la façon dont nous traitons vos informations à caractère personnel vous sera notifiée via notre Site Internet ou par email, afin que vous puissiez en prendre connaissance.

## 11. Contact

Si vous avez des questions ou des demandes concernant cette Politique ou de manière générale sur la manière dont nous traitons vos données, vous pouvez nous contacter à l’adresse email suivante : [dpo@misakey.com](mailto:dpo@misakey.com).
